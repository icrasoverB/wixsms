
<?php get_header(); ?>

<?php

$paso = 0;
if(isset($_GET['paso'])){
  $paso = $_GET['paso'];

}
?>


  <div class="header">
    <div class="device">
        <div class="row header-content">

            <div class="col-md-4 ocultar-movil">
                <div class="movil">

                </div>
            </div>

            <div class="col-md-8">

                <div class="header_info">
                      <img class="w-160" src="<?= bloginfo( 'stylesheet_directory' ); ?>/assets/img/wey_2.png" alt="">
                      <span class="title-a">SMS para Venezuela  Gratis</span>
                      <div class="w60 m-auto mt-20">
                        <span class="title-b">Envía SMS a Venezuela de forma gratis e ilimitada desde nuestro portal.</span>
                      </div>

                      <div class="publicidad728x90 ocultar-movil">

                      </div>

                      <div class="m-auto mt-20 ocultar-movil">
                        <span class="title-b">¿Por qué pagar por el envío de SMS, cuando con nosotros es gratis?</span>
                      </div>

                      <button  class="btn btn-success send-sms"><i class="fa fa-paper-plane-o" aria-hidden="true"></i> Enviar Sms</button>


                </div>

            </div>

        </div>

        </div>
    </div>
  </div>


  <div id="envio_sms">

      <div class="device">
        <div class="row header-content">

            <div class="col-md-4 ocultar-movil">
                <div class="movil-two">

                </div>
            </div>

            <div class="col-md-8">

              <div class="envio-info">

                <div class="sms_paso_1" style="<?=$paso==0 ? 'display:block;':'display:none;';?>">
                      <h2>Registrate es GARTIS!</h2>
                      <span>Para enviar mensaje de texto en el portal solo tienes que registrarte o iniciar con facebook, en el boton de abajo, que espera unete.</span>

                      <a href="<?php echo wixsms_suscribe_url_suscripcion();?>" class="block">
                        <button  class="btn btn-primary login_facebook"><i class="fa fa-facebook-square" aria-hidden="true"></i> Ingresa con tu <b>Facebook</b></button>
                      </a>

                      <div class="publicidad300x250"></div>
                </div>

                <?php
                if($paso==1){
                  if(isset($_GET['fb_id']) && $_GET['fb_id']!=''){
                    $fb_token = $_GET['fb_id'];
                  }
                ?>
                <script type="text/javascript">
                $(document).ready(function() {
                  $('html, body').animate({
                      scrollTop: $("#envio_sms").offset().top
                  }, 1000);
                });
                </script>
                <?php } ?>

                  <div class="sms_paso_2" style="<?=$paso==1 ? 'display:block;':'display:none;';?>">

                      <h2>Envía SMS Gratis</h2>
                      <span>Servicio de SMS Gratis y ilimitado</span>

                      <div class="error_datos_pais">
                          <span>Debe seleccionar un pais</span>
                          <div class="flechaAbajo">
                          </div>
                      </div>

                      <div class="error_datos_numero">
                          <span>Debe de ingresar un numero.</span>
                          <div class="flechaAbajo">
                          </div>
                      </div>
                      <div class="error_datos_mensaje">
                          <span>Ingrese un mensaje para su envio.</span>
                          <div class="flechaAbajo">
                          </div>
                      </div>


                      <form class="form-inline formulario-sms" name="addMensaje" id="addMensaje" method="post">
                          <select class="custom-select mb-2 mr-sm-2 mb-sm-0" id="pais" name="pais">
                            <option value="" selected>Pais..</option>
                            <option  value="58"> Venezuela (+58)</option>
                            <!--<option  value="52"> Mexico (+52)</option>-->
                          </select>

                          <div class="form-group">
                            <input type="text" placeholder="Numero de telefono" id="numero" name="numero" autocomplete="off" class="form-control mx-sm-3" onkeypress="return Numero(event);" maxlength="11">
                            <input type="hidden" name="facebook_token" id="facebook_token" value="<?=$fb_token;?>">
                          </div>

                          <span class="block">El número debe tener el siguiente formato: +58XXXXXXX. Otros destinos no serán entregados.</span>

                          <div class="publicidad728x90 ocultar-movil">

                          </div>

                          <div class="form-group mensaje-texto">
                            <label for="mensaje">Mensaje</label>
                            <textarea class="form-control" id="mensaje" name="mensaje" onKeyUp="return limitar(event,this.value)" onKeyDown="return limitar(event,this.value)"  rows="3" cols="38" maxlength="160"></textarea>
                            <input type="hidden" name="info" id="info" value="0">
                            <div class="g-recaptcha" data-sitekey="6LeC9xUUAAAAAC7HHNTJup-f4CxuKWrXi1J4fYKV"></div>
                            <div id="text_count">Caracteres restantes: 160</div>
                          </div>

                          <div class="block">
                            <button type="button" class="btn btn-outline-success send_sms " ><i class="fa fa-paper-plane-o" aria-hidden="true"></i> Enviar mensaje</button>
                          </div>
                      </form>
                </div>


                  <?php
                  if($paso==2){
                    if(isset($_GET['fb_id']) && $_GET['fb_id']!=''){
                      $token = $_GET['fb_id'];
                      $status = $_GET['status'];
                    }
                  ?>
                  <script type="text/javascript">
                  $(document).ready(function() {
                    $('html, body').animate({
                        scrollTop: $("#envio_sms").offset().top
                    }, 1000);
                  });
                  </script>
                  <?php } ?>

                  <div class="sms_paso_3" style="<?=$paso==2 ? 'display:block;':'display:none;';?>">
                    <div class="contenedor_pasos">

                    </div>
                    <?php
                        if($status == '200'){
                          $msj = 'Volver a enviar un sms';
                          $url = home_url('?paso=1&fb_id='.$token.'');
                    ?>
                        <div class="checked-send">
                        </div>
                        <h2 class="pt-20 mensage">!FELICIDADES! su mensaje fue enviado con exito</h2>
                        <span class="block">Regálanos un “Me Gusta”</span>
                        <span>Comparte para que tus amigos se enteren de este portal.</span>
                    <?php
                      }
                    ?>
                    <?php
                        if($status == '203' || $status=='404'){
                          $msj = 'Volver a intentarlo';
                          $url = home_url();
                    ?>
                        <div class="error-send">
                        </div>
                        <h2 class="pt-20 mensage font-1-8">!LO SIENTO! se presento un error al enviar el mensaje, contacte con el administrador.</h2>
                        <span class="block">Regálanos un “Me Gusta”</span>
                        <span>y Comparte para que tus amigos se enteren de este portal.</span>
                    <?php
                      }
                    ?>
                      <!--  <div class="cuadro-megusta">
                          <div id="fb-root"></div>
                            <script>(function(d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0];
                            if (d.getElementById(id)) return;
                            js = d.createElement(s); js.id = id;
                            js.src = "//connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v2.8&appId=232658346802041";
                            fjs.parentNode.insertBefore(js, fjs);
                            }(document, 'script', 'facebook-jssdk'));</script>
                            <input type="hidden" id="token" value="<?=$token;?>">
                            <div class="fb-like" data-href="https://www.facebook.com/ItSoftg" data-layout="button_count" data-action="like" data-size="large" data-show-faces="true" data-share="true"></div>
                        </div>-->

                        <a href="<?=$url;?>" class="block">
                            <button type="button" class="btn btn-outline-success-wix"><?=$msj;?></button>
                        </a>
                      </div>
                  </div>


            </div>
        </div>
      </div>
  </div>
  </div>

  <div id="comentarios" class="ocultar-movil">

      <div class="device">
        <div class="row header-content">

            <div class="col-md-9">

                  <div class="titulo">
                      <img src="<?= bloginfo( 'stylesheet_directory' ); ?>/assets/img/chat.png" alt="">
                      ¿Cuál es tu opinión?
                  </div>

                <!--  <div class="comment_facebook">
                    <div id="fb-root"></div>
                    <script>(function(d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id)) return;
                    js = d.createElement(s); js.id = id;
                    js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.8&appId=232658346802041";
                    fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));</script>

                    <div class="fb-comments" data-href="https://developers.facebook.com/docs/plugins/comments#configurator" data-width="728" data-numposts="2"></div>
                  </div>-->
            </div>

            <div class="col-md-3">
                <div class="publicidad300x250">

                </div>
            </div>

        </div>
      </div>


  </div>


<?php get_footer();?>
