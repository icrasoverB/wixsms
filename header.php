<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>..::: WixSms :::..</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:title" content="Weysms, envio de mensajes gratis" />
    <meta property="og:type" content="article"/>
    <meta property="og:url" content="<?php echo home_url();?>/"/>
		<meta property="og:site_name" content="itsoft"/>
    <meta property="og:description" content="Envía SMS a Venezuela de forma gratis e ilimitada desde nuestro portal." />
    <meta property="og:image" content="<?= bloginfo( 'stylesheet_directory' ); ?>/assets/img/wey.png" />

    <link rel="profile" href="http://gmpg.org/xfn/11">
    <?php do_action('wp_head'); ?>
    <script type="text/javascript">
        var url_2 = "<?php echo home_url('?paso=2');?>";
    </script>
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script>
    (adsbygoogle = window.adsbygoogle || []).push({
      google_ad_client: "ca-pub-4131487372625693",
      enable_page_level_ads: true
    });
    </script>
      <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-92830625-1', 'auto');
    ga('send', 'pageview');

  </script>
    <!--<script src='https://www.google.com/recaptcha/api.js'></script>-->
  </head>
  <body>

  <!--  <nav class="side-nav" role="navigation">

        <ul class="nav-side-nav">
          <li><a class="tooltip-side-nav" href="#section-1" title="" data-original-title="Services" data-placement="left"></a></li>
          <li><a class="tooltip-side-nav" href="#section-2" title="" data-original-title="SendSMS" data-placement="left"></a></li>
          <li><a class="tooltip-side-nav" href="#section-3" title="" data-original-title="Subscribe" data-placement="left"></a></li>
          <li><a class="tooltip-side-nav" href="#to-top" title="" data-original-title="Back" data-placement="left"></a></li>
        </ul>
      </nav>-->
