

<div class="footer_sms">
  <div class="device">
      <div class="row">

          <div class="col-sm-4">

            <h2>Sobre la Aplicación</h2>
            <p>WixSms es una herramienta que permite establecer comunicaciones directas y personalizadas a través del envío de mensajes de texto, de un máximo de 160 caracteres, desde el portal de WixSms hacia los teléfonos celulares de su preferencia. El SMS, es muy usado la comunicación de mensajes de todo tipo.</p>

            <h2>Contacto</h2>
            <div class="address">
              <span>Información: <a href="mailto:info@wixsms.com.ve">info@wixsms.com.ve</a></span>
              <span>Publicidad: <a href="mailto:info@wixsms.com.ve">publicidad@wixsms.com.ve</a></span>
            </div>

          </div>

          <div class="col-sm-4">
              <h2>Buscanos en Facebook</h2>

             <!--<div id="fb-root"></div>
              <script>(function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.8&appId=232658346802041";
                fjs.parentNode.insertBefore(js, fjs);
              }(document, 'script', 'facebook-jssdk'));</script>

              <div class="fb-page" data-href="https://www.facebook.com/ItSoftg/" data-tabs="timeline" data-width="255" data-height="230" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/ItSoftg/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/ItSoftg/">ItSoft C,A</a></blockquote></div>
            -->
          </div>

          <div class="col-sm-4">
              <h2>Términos y Condiciones de Uso</h2>
              <p>El usuario se compromete a darle un uso adecuado a la funcionalidad provista por esta aplicación, evitando contenido ofensivo. WixSms no se hace responsable por los textos malintencionados o que contengan palabras que puedan perjudicar el buen nombre u honra de las personas destinatarias. De presentarse algún incoveniente o reclamo WixSms colaborará brindando información a las autoridades correspondientes o personas perjudicadas. </p>
              <br>

                <p>2017 © WixSms. Todos los derechos reservados. </p>
          </div>

      </div>
  </div>
</div>


<?php wp_footer(); ?>
</body>
</html>
