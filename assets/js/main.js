
	    var left = 160
	    $('#text_count').text('Caracteres restantes: ' + left);
			$('#info').val(left);
	    $(document).on('keyup','#mensaje',function () {

	           left = 160 - $(this).val().length;

	            if(left < 0){
	               $('#posting').attr("disabled", true);
	            }else{
	               $('#posting').attr("disabled", false);
	             }
						$('#info').val(left);
	          $('#text_count').text('Caracteres restantes: ' + left);
	    });




// scroll top
$(document).on('click', '.top_page', function () {
    $("html, body").animate({
        scrollTop: 0
    }, 600);
    return false;
});

$(document).on('click', '.send-sms', function () {
    var href = '#envio_sms';
    var top = $(href).offset().top;
    $('html, body').animate({scrollTop: top }, 'slow');
});



// funciones paso 1
$(document).on('click','.send_sms', function(){

	if($('.formulario-sms #pais').val()==''){
			$('#pais').addClass('border-error');
			$('.error_datos_pais').fadeIn(400);
			return false;
	}
	if($('.formulario-sms #numero').val()==''){
			$('#numero').addClass('border-error');
			$('.error_datos_numero').fadeIn(400);
			return false;
	}

	if($('.formulario-sms #mensaje').val()==''){
			$('#mensaje').addClass('border-error');
			$('.error_datos_mensaje').fadeIn(400);
			return false;
	}

	$('.send_sms').html('<i class="fa fa-spinner fa-pulse fa-1x fa-fw"></i> Su mensaje se esta enviando, por favor espere...');

		$.ajax({
	    type: 'POST',
	    url: 'http://www.itsoft.com.ve/weysms/servicio/addMensaje',
	    data: $('#addMensaje').serialize(),
	    dataType: 'json',
	    success: function (data) {
				var status = data[0].status;
					if(status=='200'){
						location.href = url_2+'&fb_id='+$('#facebook_token').val()+'&status=200';
					}
					if(status=='203' || status=='404'){
						location.href = url_2+'&fb_id='+$('#facebook_token').val()+'&status='+status;
					}
	    }
		});



});

// restablecemos errores de campos
$(document).on('change','#pais',function(){
		$('#pais').removeClass('border-error');
		$('.error_datos_pais').fadeOut(400);
});
$(document).on('keydown','#numero',function(){
		$('#numero').removeClass('border-error');
		$('.error_datos_numero').fadeOut(400);
});
$(document).on('keydown','#mensaje',function(){
		$('#mensaje').removeClass('border-error');
		$('.error_datos_mensaje').fadeOut(400);
});



function Letra(evt)
{
   var nav4 = window.Event ? true : false;
	var key = nav4 ? evt.which : evt.keyCode;
	console.log(key);
	return ((key==8) ||  (key==32) || (key==0) || (key > 57) || (key > 90) && (key < 97) || (key > 122));
}

function Decimal(e)
{
  tecla=(document.all) ? e.KeyCode : e.which
  if(tecla==8)
    return true;

  letra=/[0-9.-]/;

  te=String.fromCharCode(tecla);
  return letra.test(te);
}


function Numero(evt)
{

 var nav4 = window.Event ? true : false;
var key = nav4 ? evt.which : evt.keyCode;
return (key <= 13 || (key >= 48 && key <= 57));
}

function NumLet(e)
{

  tecla = (document.all) ? e.keyCode : e.which
  if(tecla==8)
  return true;
  patron = /[0-9A-Za-z\s-]/;

  te = String.fromCharCode(tecla);

  $('#'+e.target.id+'').css({'border':'1px solid #DDD'});
  return patron.test(te);
}

function limitar(e, contenido)

        {

          var caracteres = 160;

            // obtenemos la tecla pulsada
            var unicode=e.keyCode? e.keyCode : e.charCode;
            document.getElementById('info').value= contenido.length;
            if(unicode==8 || unicode==46 || unicode==13 || unicode==9 || unicode==37 || unicode==39 || unicode==38 || unicode==40)
                return true;
            // Si ha superado el limite de caracteres devolvemos false

            if(contenido.length==caracteres)
                return false;

            return true;
        }
