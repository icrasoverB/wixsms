<?php
/**
 * Functions
 * @package WordPress
 * @subpackage sorteo
 * @since 1.0
 * @version 1.0
 */
/**
 * Si no esta ABSPATH, es debido a intentar
 * acceder a algunos datos sin permiso
 */
if (!defined('ABSPATH')) {
	die('Lo siento amigo, en cuba no nos gusta los intusos');
	exit();
}


/**
 * Registrar los estilos
 */
function tc_register_styles() {

	/* bootstrapcss */
	wp_register_style(
			'bootstrapcss', get_theme_file_uri('assets/css/bootstrap-v4/bootstrap.min.css'), false, false, 'all'
	);


	/* Font Awesome */
        wp_register_style(
                        'fontawesome', get_theme_file_uri('assets/css/font-wesome-4.6.3/font-awesome.css'), false, false, 'all'
        );


	/* style.css */
	wp_register_style(
			'style', get_stylesheet_uri(), false, false, 'all'
	);
}

/**
 * Agregar los estilos a la plantilla
 */
add_action('wp_enqueue_scripts', 'tc_enqueue_styles');

function tc_enqueue_styles() {

	/* Registrar los estilos */
	tc_register_styles();

	wp_enqueue_style('bootstrapcss');
	wp_enqueue_style('fontawesome');
	wp_enqueue_style('style');
}

/**
 * Registrar los Scripts
 */
function tc_register_scripts() {

	/* jQuery */
	wp_register_script(
			'jquery', get_theme_file_uri('assets/js/jquery.min.js'), false, false, false
	);

	/* bootstrapjs */
	wp_register_script(
			'bootstrapjs', get_theme_file_uri('assets/js/bootstrap-v4/bootstrap.min.js'), ['jquery'], false, true
	);

	/* mainjs */
	wp_register_script(
			'mainjs', get_theme_file_uri('assets/js/main.js'), ['jquery'], false, true
	);
}

/**
 * Añadir los Scripts a la plantilla
 */
add_action('wp_enqueue_scripts', 'tc_enqueue_scripts');

function tc_enqueue_scripts() {

	/* Eliminar la version de jQuery de Wordpress */
	wp_deregister_script('jquery');

	/* Registrar los Scripts */
	tc_register_scripts();



	wp_enqueue_script('jquery');
	wp_enqueue_script('bootstrapjs');
	wp_enqueue_script('mainjs');
	wp_localize_script( 'mainjs', 'admin_ajax', admin_url( 'admin-ajax.php' ) );
}
